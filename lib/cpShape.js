const CP_NO_GROUP                = 0;
const CP_ALL_CATEGORIES          = (~0);
const CP_WILDCARD_COLLISION_TYPE = (~0);

const cpShapeFilter = function cpShapeFilter(group, categories, mask)
{
    this.group = group;
    this.categories = categories;
    this.mask = mask;
}
const cpShapeFilterNew = function cpShapeFilterNew(group, categories, mask)
{
    return new cpShapeFilter(group, categories, mask);
}

cpShapeFilter.prototype.group;
cpShapeFilter.prototype.categories;
cpShapeFilter.prototype.mask;

const CP_SHAPE_FILTER_ALL  = new cpShapeFilter(CP_NO_GROUP, CP_ALL_CATEGORIES, CP_ALL_CATEGORIES);
const CP_SHAPE_FILTER_NONE = new cpShapeFilter(CP_NO_GROUP, ~CP_ALL_CATEGORIES, ~CP_ALL_CATEGORIES);

exports.CP_NO_GROUP = CP_NO_GROUP;
exports.CP_ALL_CATEGORIES = CP_ALL_CATEGORIES;
exports.CP_WILDCARD_COLLISION_TYPE = CP_WILDCARD_COLLISION_TYPE;

exports.cpShapeFilter = cpShapeFilter;
exports.cpShapeFilterNew = cpShapeFilterNew;

exports.CP_SHAPE_FILTER_ALL = CP_SHAPE_FILTER_ALL;
exports.CP_SHAPE_FILTER_NONE = CP_SHAPE_FILTER_NONE;

