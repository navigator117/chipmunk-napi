const chipmunk = require('bindings')('chipmunk_napi');
const cpvlib = require('./cpVect');
const cpslib = require('./cpShape');

chipmunk.CP_VERSION_MAJOR = 7;
chipmunk.CP_VERSION_MINOR = 0;
chipmunk.CP_VERSION_RELEASE = 1;

chipmunk.CP_BODY_TYPE_DYNAMIC   = 0;
chipmunk.CP_BODY_TYPE_KINEMATIC = 1;
chipmunk.CP_BODY_TYPE_STATIC    = 2;

chipmunk.cpAreaForCircle = function cpAreaForCircle(r1, r2)
{
	return cpvlib.CP_PI * cpvlib.cpfabs(r1*r1 - r2*r2);
}

if (typeof global !== 'undefined') {
    global.cpVect = cpvlib.cpVect;
    global.cpShapeFilter = cpslib.cpShapeFilter;    
}
else if (typeof window !== 'undefined') {
    window.cpVect = cpvlib.cpVect;
    window.cpShapeFilter = cpslib.cpShapeFilter;
}
else if (typeof self !== 'undefined') {
    self.cpVect = cpvlib.cpVect;
    self.cpShapeFilter = cpslib.cpShapeFilter;    
}

exports.chipmunk = chipmunk;
exports.cpvlib = cpvlib;
exports.cpslib = cpslib;
