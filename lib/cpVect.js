const CPFLOAT_MIN = 1e-37;
const CP_PI = Math.PI;
const cpfabs = Math.abs;
const cpfsqrt = Math.sqrt;
const cpfsin = Math.sin;
const cpfcos = Math.cos;
const cpfacos = Math.acos;
const cpfatan2 = Math.atan2;
// const cpfmod = Math.fmod;
const cpfexp = Math.exp;
const cpfpow = Math.pow;
const cpffloor = Math.floor;
const cpfceil = Math.ceil;

const cpVect = function cpVect(x, y)
{
    this.x = x;
    this.y = y;
}

cpVect.prototype.x;
cpVect.prototype.y;

const cpvzero = new cpVect(0.0, 0.0);

const cpv = function cpv(x, y)
{
    return new cpVect(x, y);
}

const cpveql = function cpveql(v1, v2)
{
	return (v1.x == v2.x && v1.y == v2.y);
}

const cpvadd = function cpvadd(v1, v2)
{
	return cpv(v1.x + v2.x, v1.y + v2.y);
}

const cpvsub = function cpvsub(v1, v2)
{
	return cpv(v1.x - v2.x, v1.y - v2.y);
}

const cpvneg = function cpvneg(v)
{
	return cpv(-v.x, -v.y);
}

const cpvmult = function cpvmult(v, s)
{
	return cpv(v.x*s, v.y*s);
}

const cpvdot = function cpvdot(v1, v2)
{
	return v1.x*v2.x + v1.y*v2.y;
}

const cpvcross = function cpvcross(v1, v2)
{
	return v1.x*v2.y - v1.y*v2.x;
}

const cpvperp = function cpvperp(v)
{
	return cpv(-v.y, v.x);
}

const cpvrperp = function cpvrperp(v)
{
	return cpv(v.y, -v.x);
}

const cpvproject = function cpvproject(v1, v2)
{
	return cpvmult(v2, cpvdot(v1, v2)/cpvdot(v2, v2));
}

const cpvforangle = function cpvforangle(a)
{
	return cpv(cpfcos(a), cpfsin(a));
}

const cpvtoangle = function cpvtoangle(v)
{
	return cpfatan2(v.y, v.x);
}

const cpvrotate = function cpvrotate(v1, v2)
{
	return cpv(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);
}

const cpvunrotate = function cpvunrotate(v1, v2)
{
	return cpv(v1.x*v2.x + v1.y*v2.y, v1.y*v2.x - v1.x*v2.y);
}

const cpvlengthsq = function cpvlengthsq(v)
{
	return cpvdot(v, v);
}

const cpvlength = function cpvlength(v)
{
	return cpfsqrt(cpvdot(v, v));
}

const cpvlerp = function cpvlerp(v1, v2, t)
{
	return cpvadd(cpvmult(v1, 1.0 - t), cpvmult(v2, t));
}

const cpvnormalize = function cpvnormalize(v)
{
	// Neat trick I saw somewhere to avoid div/0.
	return cpvmult(v, 1.0/(cpvlength(v) + CPFLOAT_MIN));
}

const cpvslerp = function cpvslerp(v1, v2, t)
{
	var dot = cpvdot(cpvnormalize(v1), cpvnormalize(v2));
	var omega = cpfacos(cpfclamp(dot, -1.0, 1.0));	
	if(omega < 1e-3) {
		// If the angle between two vectors is very small, lerp instead to avoid precision issues.
		return cpvlerp(v1, v2, t);
	}
    else {
		var denom = 1.0/cpfsin(omega);
		return cpvadd(cpvmult(v1, cpfsin((1.0 - t)*omega)*denom), cpvmult(v2, cpfsin(t*omega)*denom));
	}
}

const cpvslerpconst = function cpvslerpconst(v1, v2, a)
{
	var dot = cpvdot(cpvnormalize(v1), cpvnormalize(v2));
	var omega = cpfacos(cpfclamp(dot, -1.0, 1.0));
	
	return cpvslerp(v1, v2, cpfmin(a, omega)/omega);
}

const cpvclamp = function cpvclamp(v, len)
{
	return (cpvdot(v,v) > len*len) ? cpvmult(cpvnormalize(v), len) : v;
}

const cpvlerpconst = function cpvlerpconst(v1, v2, d)
{
	return cpvadd(v1, cpvclamp(cpvsub(v2, v1), d));
}

const cpvdist = function cpvdist(v1, v2)
{
	return cpvlength(cpvsub(v1, v2));
}

/// Returns the squared distance between v1 and v2. Faster than cpvdist() when you only need to compare distances.
const cpvdistsq = function cpvdistsq(v1, v2)
{
	return cpvlengthsq(cpvsub(v1, v2));
}

/// Returns true if the distance between v1 and v2 is less than dist.
const cpvnear = function cpvnear(v1, v2, dist)
{
	return cpvdistsq(v1, v2) < dist*dist;
}

exports.CPFLOAT_MIN = CPFLOAT_MIN;
exports.CP_PI = CP_PI;
exports.cpfabs = Math.abs;
exports.cpfsqrt = cpfsqrt;
exports.cpfsin = cpfsin;
exports.cpfcos = cpfcos;
exports.cpfacos = cpfacos;
exports.cpfatan2 = cpfatan2;
// exports.cpfmod = Math.fmod;
exports.cpfexp = cpfexp;
exports.cpfpow = cpfpow;
exports.cpffloor = cpffloor;
exports.cpfceil = cpfceil;

exports.cpVect = cpVect;

exports.cpvzero = cpvzero;
exports.cpv = cpv;
exports.cpveql = cpveql;
exports.cpvadd = cpvadd;
exports.cpvsub = cpvsub;
exports.cpvneg = cpvneg;
exports.cpvmult = cpvmult;
exports.cpvdot = cpvdot;
exports.cpvcross = cpvcross;
exports.cpvperp = cpvperp;
exports.cpvrperp = cpvrperp;
exports.cpvproject = cpvproject;
exports.cpvforangle = cpvforangle;
exports.cpvtoangle = cpvtoangle;
exports.cpvrotate = cpvrotate;
exports.cpvunrotate = cpvunrotate;
exports.cpvlengthsq = cpvlengthsq;
exports.cpvlength = cpvlength;
exports.cpvlerp = cpvlerp;
exports.cpvnormalize = cpvnormalize;
exports.cpvslerp = cpvslerp;
exports.cpvslerpconst = cpvslerpconst;
exports.cpvclamp = cpvclamp;
exports.cpvlerpconst = cpvlerpconst;
exports.cpvdist = cpvdist;
exports.cpvdistsq = cpvdistsq;
exports.cpvnear = cpvnear;
