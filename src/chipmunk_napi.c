#include <node_api.h>
#include <alloca.h>
#include <stdio.h>
#include <chipmunk/chipmunk_private.h>
#include <chipmunk/chipmunk.h>
#include <chipmunk/chipmunk_unsafe.h>

static napi_env global_env;

napi_value
chipmunk_cpBodyNew(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    napi_value result;
    cpFloat m, i;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_double(env, argv[0], &m)) != napi_ok ||
        (status=napi_get_value_double(env, argv[1], &i)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid m or i!");        
    }
    body = cpBodyNew(m, i);
    if ((status=napi_create_external(env, body, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpBodyNewStatic(napi_env env, napi_callback_info info)
{
    cpBody* body = cpBodyNewStatic();
    napi_status status;    
    napi_value result;
    if ((status=napi_create_external(env, body, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpBodyNewKinematic(napi_env env, napi_callback_info info)
{
    cpBody* body = cpBodyNewKinematic();
    napi_status status;    
    napi_value result;
    if ((status=napi_create_external(env, body, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpBodyFree(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    cpBodyFree(body);
    return NULL;
}

napi_value
chipmunk_cpBodySetUserData(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    napi_ref result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_create_reference(env, argv[1], 0, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid userData!");
    }
    cpBodySetUserData(body, (cpDataPointer)result);
    return NULL;
}

napi_value
chipmunk_cpBodyGetUserData(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_ref ref;
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    ref = (napi_ref)cpBodyGetUserData(body);
    if ((status=napi_get_reference_value(env, ref, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpBodyGetPosition(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect position = cpvzero;
    napi_value napi_value_global, napi_position[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    position = cpBodyGetPosition(body);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, position.x, &napi_position[0])) != napi_ok ||
        (status=napi_create_double(env, position.y, &napi_position[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_position, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect position!");
    }
    return result;
}

napi_value
chipmunk_cpBodySetPosition(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect position = cpvzero;
    napi_value napi_position_x;
    napi_value napi_position_y;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_position_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_position_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_position_x, &position.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_position_y, &position.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid position!");
    }
    cpBodySetPosition(body, position);
    return NULL;
}

napi_value
chipmunk_cpBodyGetVelocity(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect velocity = cpvzero;
    napi_value napi_value_global, napi_velocity[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    velocity = cpBodyGetVelocity(body);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }    
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, velocity.x, &napi_velocity[0])) != napi_ok ||
        (status=napi_create_double(env, velocity.y, &napi_velocity[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_velocity, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect velocity!");
    }
    return result;
}

napi_value
chipmunk_cpBodySetVelocity(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect velocity = cpvzero;
    napi_value napi_velocity_x;
    napi_value napi_velocity_y;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_velocity_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_velocity_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_velocity_x, &velocity.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_velocity_y, &velocity.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid velocity!");
    }
    cpBodySetVelocity(body, velocity);
    return NULL;
}

napi_value
chipmunk_cpBodyGetMass(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat mass = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if (body == NULL) {
        napi_throw_error(env, NULL, "invalid body!");        
    }
    mass = cpBodyGetMass(body);
    if ((status=napi_create_double(env, mass, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create mass value!");
    }
    return result;
}

napi_value
chipmunk_cpBodySetMass(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat mass = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_value_double(env, argv[1], &mass) != napi_ok)){
        napi_throw_error(env, NULL, "invalid mass!");
    }
    cpBodySetMass(body, mass);
    return NULL;
}

napi_value
chipmunk_cpBodyGetAngle(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat angle = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    angle = cpBodyGetAngle(body);
    if ((status=napi_create_double(env, angle, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create angle value!");
    }
    return result;
}

napi_value
chipmunk_cpBodySetAngle(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat angle = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_value_double(env, argv[1], &angle) != napi_ok)){
        napi_throw_error(env, NULL, "invalid angle!");
    }
    cpBodySetAngle(body, angle);
    return NULL;
}

napi_value
chipmunk_cpBodyGetAngularVelocity(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat angularVelocity = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    angularVelocity = cpBodyGetAngularVelocity(body);
    if ((status=napi_create_double(env, angularVelocity, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create angularVelocity value!");
    }
    return result;
}

napi_value
chipmunk_cpBodySetAngularVelocity(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat angularVelocity = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_value_double(env, argv[1], &angularVelocity) != napi_ok)){
        napi_throw_error(env, NULL, "invalid angularVelocity!");
    }
    cpBodySetAngularVelocity(body, angularVelocity);
    return NULL;
}

napi_value
chipmunk_cpBodyGetType(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    int32_t type;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    type = (int32_t)cpBodyGetType(body);
    if ((status=napi_create_int32(env, type, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create type value!");
    }
    return result;
}

napi_value
chipmunk_cpBodySetType(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    int32_t type;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_value_int32(env, argv[1], &type) != napi_ok)){
        napi_throw_error(env, NULL, "invalid type!");
    }
    cpBodySetType(body, (cpBodyType)type);
    return NULL;
}

napi_value
chipmunk_cpBodyIsSleeping(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    _Bool sleeping = false;
    napi_status status;
    size_t argc = 1;
    napi_value napi_value_true, napi_value_false, argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    sleeping = (_Bool)cpBodyIsSleeping(body);
    if ((status=napi_get_boolean(env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_true!");
    }
    if ((status=napi_get_boolean(env, cpFalse, &napi_value_false)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_false!");
    }
    result = (sleeping ? napi_value_true : napi_value_false);
    return result;
}

napi_value
chipmunk_cpBodyActivate(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    cpBodyActivate(body);
    return NULL;
}

napi_value
chipmunk_cpBodySleep(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    cpBodySleep(body);
    return NULL;
}

napi_value
chipmunk_cpBodyUpdateVelocity(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect gravity = cpvzero;
    cpFloat damping = 0.0, dt = 0.0;
    napi_value napi_gravity_x;
    napi_value napi_gravity_y;
    napi_status status;
    size_t argc = 4;
    napi_value argv[4];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_gravity_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_gravity_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_gravity_x, &gravity.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_gravity_y, &gravity.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid gravity!");
    }
    if ((status=napi_get_value_double(env, argv[2], &damping) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid damping!");
    }
    if ((status=napi_get_value_double(env, argv[3], &dt) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid dt!");
    }
    cpBodyUpdateVelocity(body, gravity, damping, dt);
    return NULL;
}

#define PROP_NAME_VELOCITY_UPDATE_FUNC ("__VELOCITY_UPDATE_FUNC__")
#define PROP_NAME_VELOCITY_UPDATE_THIS ("__VELOCITY_UPDATE_THIS__")

static void
chipmunk_cpBodyVelocityUpdateFunc(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt)
{
    napi_status status;
    napi_ref ref;
    napi_value napi_value_global, napi_user_data;
    napi_value napi_velocity_update_func;
    napi_value napi_velocity_update_this;
    napi_value napi_gravity[2];
    int argc = 4;
    napi_value argv[4];
    napi_value constructor, result;
    ref = (napi_ref)cpBodyGetUserData(body);
    if ((status=napi_get_reference_value(global_env, ref, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, PROP_NAME_VELOCITY_UPDATE_FUNC,  &napi_velocity_update_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "load callback func failed!");            
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, PROP_NAME_VELOCITY_UPDATE_THIS,  &napi_velocity_update_this)) != napi_ok) {
        napi_throw_error(global_env, NULL, "load callback this failed!");
    }
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }    
    if ((status=napi_get_named_property(global_env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_external(global_env, body, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external body!");
    }
    if ((status=napi_create_double(global_env, gravity.x, &napi_gravity[0])) != napi_ok ||
        (status=napi_create_double(global_env, gravity.y, &napi_gravity[1])) != napi_ok ||
        (status=napi_new_instance(global_env, constructor, 2, napi_gravity, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create cpVect gravity!");
    }
    if ((status=napi_create_double(global_env, damping, &argv[2])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create damping!");        
    }
    if ((status=napi_create_double(global_env, dt, &argv[3])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create dt!");
    }
    if ((status=napi_call_function(global_env, napi_velocity_update_this, napi_velocity_update_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
}

napi_value
chipmunk_cpBodySetVelocityUpdateFunc(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3], napi_value_null;
    _Bool is_null = cpFalse;
    napi_ref ref;
    napi_value napi_user_data;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_null(global_env, &napi_value_null)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_null!");
    }    
    if ((status=napi_strict_equals(env, argv[1], napi_value_null, &is_null)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid callback!");
    }
    ref = (napi_ref)cpBodyGetUserData(body);
    if ((status=napi_get_reference_value(env, ref, &napi_user_data)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid user data!");
    }
    if (!is_null) {
        if ((status=napi_set_named_property(env, napi_user_data, PROP_NAME_VELOCITY_UPDATE_FUNC,  argv[1])) != napi_ok) {
            napi_throw_error(env, NULL, "save callback func failed!");            
        }
        if ((status=napi_set_named_property(env, napi_user_data, PROP_NAME_VELOCITY_UPDATE_THIS,  argv[2])) != napi_ok) {
            napi_throw_error(env, NULL, "save callback this failed!");
        }
        cpBodySetVelocityUpdateFunc(body, chipmunk_cpBodyVelocityUpdateFunc);
    }
    else {
        cpBodySetVelocityUpdateFunc(body, cpBodyUpdateVelocity);
    }
    return NULL;
}

napi_value
chipmunk_cpBodyUpdatePosition(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpFloat dt = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_value_double(env, argv[1], &dt) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid dt!");
    }
    cpBodyUpdatePosition(body, dt);
    return NULL;
}

#define PROP_NAME_POSITION_UPDATE_FUNC ("__POSITION_UPDATE_FUNC__")
#define PROP_NAME_POSITION_UPDATE_THIS ("__POSITION_UPDATE_THIS__")

static void
chipmunk_cpBodyPositionUpdateFunc(cpBody *body, cpFloat dt)
{
    napi_status status;
    napi_ref ref;
    napi_value napi_value_global, napi_user_data;
    napi_value napi_position_update_func;
    napi_value napi_position_update_this;
    int argc = 2;
    napi_value argv[2];
    napi_value constructor, result;
    ref = (napi_ref)cpBodyGetUserData(body);
    if ((status=napi_get_reference_value(global_env, ref, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, PROP_NAME_POSITION_UPDATE_FUNC,  &napi_position_update_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "load callback func failed!");            
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, PROP_NAME_POSITION_UPDATE_THIS,  &napi_position_update_this)) != napi_ok) {
        napi_throw_error(global_env, NULL, "load callback this failed!");
    }
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }    
    if ((status=napi_get_named_property(global_env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_external(global_env, body, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external body!");
    }
    if ((status=napi_create_double(global_env, dt, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create dt!");
    }
    if ((status=napi_call_function(global_env, napi_position_update_this, napi_position_update_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
}

napi_value
chipmunk_cpBodySetPositionUpdateFunc(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3], napi_value_null;
    _Bool is_null = cpFalse;
    napi_ref ref;
    napi_value napi_user_data;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_null(global_env, &napi_value_null)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_null!");
    }
    if ((status=napi_strict_equals(env, argv[1], napi_value_null, &is_null)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid callback!");
    }
    ref = (napi_ref)cpBodyGetUserData(body);
    if ((status=napi_get_reference_value(env, ref, &napi_user_data)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid user data!");
    }
    if (!is_null) {
        if ((status=napi_set_named_property(env, napi_user_data, PROP_NAME_POSITION_UPDATE_FUNC,  argv[1])) != napi_ok) {
            napi_throw_error(env, NULL, "save callback func failed!");
        }
        if ((status=napi_set_named_property(env, napi_user_data, PROP_NAME_POSITION_UPDATE_THIS,  argv[2])) != napi_ok) {
            napi_throw_error(env, NULL, "save callback this failed!");
        }
        cpBodySetPositionUpdateFunc(body, chipmunk_cpBodyPositionUpdateFunc);
    }
    else {
        cpBodySetPositionUpdateFunc(body, cpBodyUpdatePosition);
    }
    return NULL;
}

napi_value
chipmunk_cpBodyApplyForceAtWorldPoint(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect force = cpvzero, point = cpvzero;
    napi_value napi_x;
    napi_value napi_y;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &force.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &force.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid force!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &point.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &point.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid point!");
    }
    cpBodyApplyForceAtWorldPoint(body, force, point);
    return NULL;
}

napi_value
chipmunk_cpBodyApplyForceAtLocalPoint(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect force = cpvzero, point = cpvzero;
    napi_value napi_x;
    napi_value napi_y;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &force.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &force.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid force!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &point.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &point.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid point!");
    }
    cpBodyApplyForceAtLocalPoint(body, force, point);
    return NULL;
}

napi_value
chipmunk_cpBodyApplyImpulseAtWorldPoint(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect impulse = cpvzero, point = cpvzero;
    napi_value napi_x;
    napi_value napi_y;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &impulse.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &impulse.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid impulse!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &point.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &point.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid point!");
    }
    cpBodyApplyImpulseAtWorldPoint(body, impulse, point);
    return NULL;
}

napi_value
chipmunk_cpBodyApplyImpulseAtLocalPoint(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpVect impulse = cpvzero, point = cpvzero;
    napi_value napi_x;
    napi_value napi_y;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &impulse.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &impulse.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid impulse!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_x, &point.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_y, &point.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid point!");
    }
    cpBodyApplyImpulseAtLocalPoint(body, impulse, point);
    return NULL;
}
            
napi_value
chipmunk_cpSpaceNew(napi_env env, napi_callback_info info)
{
    cpSpace* space = cpSpaceNew();
    napi_status status;    
    napi_value result;
    if ((status=napi_create_external(env, space, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpSpaceContainBody(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpBody* body = NULL;
    _Bool contain = false;
    napi_status status;
    size_t argc = 2;
    napi_value napi_value_true, napi_value_false, argv[2];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    contain = (_Bool)cpSpaceContainsBody(space, body);
    if ((status=napi_get_boolean(env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_true!");
    }
    if ((status=napi_get_boolean(env, cpFalse, &napi_value_false)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_false!");
    }
    result = (contain ? napi_value_true : napi_value_false);
    return result;
}

napi_value
chipmunk_cpSpaceContainShape(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpShape* shape = NULL;    
    _Bool contain = false;
    napi_status status;
    size_t argc = 2;
    napi_value napi_value_true, napi_value_false, argv[2];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }    
    contain = (_Bool)cpSpaceContainsShape(space, shape);
    if ((status=napi_get_boolean(env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_true!");
    }
    if ((status=napi_get_boolean(env, cpFalse, &napi_value_false)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_false!");
    }
    result = (contain ? napi_value_true : napi_value_false);
    return result;
}

napi_value
chipmunk_cpSpaceGetGravity(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpVect gravity = cpvzero;
    napi_value napi_value_global, napi_gravity[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    gravity = cpSpaceGetGravity(space);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }    
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, gravity.x, &napi_gravity[0])) != napi_ok ||
        (status=napi_create_double(env, gravity.y, &napi_gravity[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_gravity, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect gravity!");
    }
    return result;
}

napi_value
chipmunk_cpSpaceSetGravity(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpVect gravity = cpvzero;
    napi_value napi_gravity_x;
    napi_value napi_gravity_y;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_gravity_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_gravity_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_gravity_x, &gravity.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_gravity_y, &gravity.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid gravity!");
    }
    cpSpaceSetGravity(space, gravity);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetDamping(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat damping = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    damping = cpSpaceGetDamping(space);
    if ((status=napi_create_double(env, damping, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create damping value!");
    }    
    return result;
}

napi_value
chipmunk_cpSpaceSetDamping(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat damping = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_double(env, argv[1], &damping) != napi_ok)){
        napi_throw_error(env, NULL, "invalid damping!");
    }
    cpSpaceSetDamping(space, damping);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetIdleSpeedThreshold(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat idleSpeedThreshold = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    idleSpeedThreshold = cpSpaceGetIdleSpeedThreshold(space);
    if ((status=napi_create_double(env, idleSpeedThreshold, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create idleSpeedThreshold value!");
    }    
    return result;
}

napi_value
chipmunk_cpSpaceSetIdleSpeedThreshold(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat idleSpeedThreshold = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_double(env, argv[1], &idleSpeedThreshold) != napi_ok)){
        napi_throw_error(env, NULL, "invalid idleSpeedThreshold!");
    }
    cpSpaceSetIdleSpeedThreshold(space, idleSpeedThreshold);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetSleepTimeThreshold(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat sleepTimeThreshold = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    sleepTimeThreshold = cpSpaceGetSleepTimeThreshold(space);
    if ((status=napi_create_double(env, sleepTimeThreshold, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create sleepTimeThreshold value!");
    }    
    return result;
}

napi_value
chipmunk_cpSpaceSetSleepTimeThreshold(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat sleepTimeThreshold = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_double(env, argv[1], &sleepTimeThreshold) != napi_ok)){
        napi_throw_error(env, NULL, "invalid sleepTimeThreshold!");
    }
    cpSpaceSetSleepTimeThreshold(space, sleepTimeThreshold);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetCollisionSlop(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat collisionSlop = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    collisionSlop = cpSpaceGetCollisionSlop(space);
    if ((status=napi_create_double(env, collisionSlop, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create collisionSlop value!");
    }    
    return result;
}

napi_value
chipmunk_cpSpaceSetCollisionSlop(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat collisionSlop = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_double(env, argv[1], &collisionSlop) != napi_ok)){
        napi_throw_error(env, NULL, "invalid collisionSlop!");
    }
    cpSpaceSetCollisionSlop(space, collisionSlop);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetCollisionBias(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat collisionBias = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    collisionBias = cpSpaceGetCollisionBias(space);
    if ((status=napi_create_double(env, collisionBias, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create collisionBias value!");
    }    
    return result;
}

napi_value
chipmunk_cpSpaceSetCollisionBias(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat collisionBias = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_double(env, argv[1], &collisionBias) != napi_ok)){
        napi_throw_error(env, NULL, "invalid collisionBias!");
    }
    cpSpaceSetCollisionBias(space, collisionBias);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetIterations(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    int32_t iterations;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    iterations = cpSpaceGetIterations(space);
    if ((status=napi_create_int32(env, iterations, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create iterations value!");
    }
    return result;
}

napi_value
chipmunk_cpSpaceSetIterations(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    int32_t iterations;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_int32(env, argv[1], &iterations) != napi_ok)){
        napi_throw_error(env, NULL, "invalid iterations!");
    }
    cpSpaceSetIterations(space, iterations);
    return NULL;
}

napi_value
chipmunk_cpSpaceSetUserData(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    napi_ref result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_create_reference(env, argv[1], 0, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid userData!");
    }
    cpSpaceSetUserData(space, (cpDataPointer)result);
    return NULL;
}

napi_value
chipmunk_cpSpaceGetUserData(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_ref ref;
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    ref = (napi_ref)cpSpaceGetUserData(space);
    if ((status=napi_get_reference_value(env, ref, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

#define PROP_NAME_POST_STEP_CALLBACK_FUNC ("__POST_STEP_CALLBACK_FUNC__")

static void
chipmunk_cpSpacePostStepCallbackFunc(cpSpace* space, void* key, void* data)
{
    napi_status status;
    napi_ref ref_key, ref_data;
    napi_value napi_key, napi_data;
    napi_value napi_value_global, napi_poststep_callback_func;    
    int argc = 3;
    napi_value argv[3];
    napi_value result;
    ref_key = (napi_ref)key;
    ref_data = (napi_ref)data;
    if ((status=napi_get_reference_value(global_env, ref_key, &napi_key)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid key!");
    }
    if ((status=napi_get_reference_value(global_env, ref_data, &napi_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid data!");
    }
    if ((status=napi_get_named_property(global_env, napi_key, PROP_NAME_POST_STEP_CALLBACK_FUNC, &napi_poststep_callback_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "load callback func failed!");
    }
    if ((status=napi_create_external(global_env, space, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external space!");
    }
    argv[1] = napi_key;
    argv[2] = napi_data;
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_call_function(global_env, napi_value_global, napi_poststep_callback_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
}

napi_value
chipmunk_cpSpaceAddPostStepCallback(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    napi_status status;
    size_t argc = 4;
    napi_value argv[4];
    napi_ref ref_key, ref_data;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_create_reference(env, argv[2], 0, &ref_key)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid key!");
    }
    if ((status=napi_create_reference(env, argv[3], 0, &ref_data)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid data!");
    }    
    if ((status=napi_set_named_property(env, argv[2], PROP_NAME_POST_STEP_CALLBACK_FUNC,  argv[1])) != napi_ok) {
        napi_throw_error(env, NULL, "save callback func failed!");
    }
    cpSpaceAddPostStepCallback(space, chipmunk_cpSpacePostStepCallbackFunc, (void*)ref_key, (void*)ref_data);
    return NULL;
}

static cpBool
chipmunk_cpCollisionBeginFunc(cpArbiter* arbiter, cpSpace* space, void* userData)
{
    napi_status status;    
    napi_value napi_value_global, napi_value_true, napi_user_data, napi_callback_func;
    napi_ref ref_user_data = (napi_ref)userData;
    int argc = 3;
    napi_value argv[3];
    napi_value result;
    _Bool bool_result = cpFalse;
    if ((status=napi_get_reference_value(global_env, ref_user_data, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, "beginFunc", &napi_callback_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid callback beginFunc!");
    }
    if ((status=napi_create_external(global_env, arbiter, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external arbiter!");
    }            
    if ((status=napi_create_external(global_env, space, NULL, NULL, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external space!");
    }
    argv[2] = napi_user_data;
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }    
    if ((status=napi_call_function(global_env, napi_value_global, napi_callback_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
    if ((status=napi_get_boolean(global_env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_true!");
    }    
    if ((status=napi_strict_equals(global_env, napi_value_true, result, &bool_result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't compare result with boolean true!");
    }
    return bool_result;
}

static cpBool
chipmunk_cpCollisionPreSolveFunc(cpArbiter* arbiter, cpSpace* space, void* userData)
{
    napi_status status;    
    napi_value napi_value_global, napi_value_true, napi_user_data, napi_callback_func;
    napi_ref ref_user_data = (napi_ref)userData;
    int argc = 3;
    napi_value argv[3];
    napi_value result;
    _Bool bool_result = cpFalse;
    if ((status=napi_get_reference_value(global_env, ref_user_data, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, "preSolveFunc", &napi_callback_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid callback preSolveFunc!");
    }
    if ((status=napi_create_external(global_env, arbiter, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external arbiter!");
    }            
    if ((status=napi_create_external(global_env, space, NULL, NULL, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external space!");
    }
    argv[2] = napi_user_data;
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }    
    if ((status=napi_call_function(global_env, napi_value_global, napi_callback_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
    if ((status=napi_get_boolean(global_env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_true!");
    }    
    if ((status=napi_strict_equals(global_env, napi_value_true, result, &bool_result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't compare result with boolean true!");
    }
    return bool_result;
}

static void
chipmunk_cpCollisionPostSolveFunc(cpArbiter* arbiter, cpSpace* space, void* userData)
{
    napi_status status;    
    napi_value napi_value_global, napi_user_data, napi_callback_func;
    napi_ref ref_user_data = (napi_ref)userData;
    int argc = 3;
    napi_value argv[3];
    napi_value result;
    if ((status=napi_get_reference_value(global_env, ref_user_data, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, "postSolveFunc", &napi_callback_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid callback postSolveFunc!");
    }
    if ((status=napi_create_external(global_env, arbiter, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external arbiter!");
    }            
    if ((status=napi_create_external(global_env, space, NULL, NULL, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external space!");
    }
    argv[2] = napi_user_data;
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }        
    if ((status=napi_call_function(global_env, napi_value_global, napi_callback_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
}

static void
chipmunk_cpCollisionSeparateFunc(cpArbiter* arbiter, cpSpace* space, void* userData)
{
    napi_status status;    
    napi_value napi_value_global, napi_user_data, napi_callback_func;
    napi_ref ref_user_data = (napi_ref)userData;
    int argc = 3;
    napi_value argv[3];
    napi_value result;
    if ((status=napi_get_reference_value(global_env, ref_user_data, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    if ((status=napi_get_named_property(global_env, napi_user_data, "separateFunc", &napi_callback_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid callback separateFunc!");
    }
    if ((status=napi_create_external(global_env, arbiter, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external arbiter!");
    }            
    if ((status=napi_create_external(global_env, space, NULL, NULL, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create external space!");
    }
    argv[2] = napi_user_data;
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_call_function(global_env, napi_value_global, napi_callback_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
}

napi_value
chipmunk_cpSpaceAddCollisionHandler(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    uint32_t collisionTypeA = (uint32_t)CP_WILDCARD_COLLISION_TYPE;
    uint32_t collisionTypeB = (uint32_t)CP_WILDCARD_COLLISION_TYPE;
    napi_status status;
    size_t argc = 4;
    napi_value argv[4], napi_value_undefined, napi_callback_func;
    napi_ref ref_user_data;
    _Bool is_undefined = cpFalse;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_uint32(env, argv[1], &collisionTypeA) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid collisionTypeA!");
    }
    if ((status=napi_get_value_uint32(env, argv[2], &collisionTypeB) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid collisionTypeB!");
    }
    if ((status=napi_create_reference(env, argv[3], 1, &ref_user_data)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid userData!");
    }
    cpCollisionHandler* collisionHandler =
        cpSpaceAddCollisionHandler(space, collisionTypeA, collisionTypeB);
    if (collisionHandler != NULL) {
        if ((status=napi_get_undefined(global_env, &napi_value_undefined)) != napi_ok) {
            napi_throw_error(global_env, NULL, "can't get napi_value_undefined!");
        }
        if ((status=napi_get_named_property(env, argv[3], "beginFunc", &napi_callback_func)) == napi_ok) {
            if ((status=napi_strict_equals(env, napi_callback_func, napi_value_undefined, &is_undefined)) != napi_ok) {
                napi_throw_error(env, NULL, "invalid callback value!");
            }
            if (!is_undefined) {
                collisionHandler->beginFunc = chipmunk_cpCollisionBeginFunc;
            }
        }
        if ((status=napi_get_named_property(env, argv[3], "preSolveFunc", &napi_callback_func)) == napi_ok) {
            if ((status=napi_strict_equals(env, napi_callback_func, napi_value_undefined, &is_undefined)) != napi_ok) {
                napi_throw_error(env, NULL, "invalid callback value!");
            }
            if (!is_undefined) {
                collisionHandler->preSolveFunc = chipmunk_cpCollisionPreSolveFunc;
            }
        }
        if ((status=napi_get_named_property(env, argv[3], "postSolveFunc", &napi_callback_func)) == napi_ok) {
            if ((status=napi_strict_equals(env, napi_callback_func, napi_value_undefined, &is_undefined)) != napi_ok) {
                napi_throw_error(env, NULL, "invalid callback value!");
            }
            if (!is_undefined) {
                collisionHandler->postSolveFunc = chipmunk_cpCollisionPostSolveFunc;
            }
        }        
        if ((status=napi_get_named_property(env, argv[3], "separateFunc", &napi_callback_func)) == napi_ok) {
            if ((status=napi_strict_equals(env, napi_callback_func, napi_value_undefined, &is_undefined)) != napi_ok) {
                napi_throw_error(env, NULL, "invalid callback value!");
            }
            if (!is_undefined) {            
                collisionHandler->separateFunc = chipmunk_cpCollisionSeparateFunc;
            }
        }
        collisionHandler->userData = (cpDataPointer)ref_user_data;
    }
    return NULL;
}

#define PROP_NAME_SPACE_POINT_QUERY_FUNC ("__SPACE_POINT_QUERY_FUNC__")

void
chipmunk_cpSpacePointQueryCallbackFunc(cpShape* shape, cpVect point, cpFloat distance, cpVect gradient, void* data)
{
    size_t argc = 5;
    napi_value argv[5], constructor, napi_callback_func;
    napi_value napi_value_global, napi_point[2], napi_gradient[2];
    napi_ref ref_user_data;
    napi_value napi_user_data, result;
    napi_status status;    
    if ((status=napi_create_external(global_env, shape, NULL, NULL, &argv[0])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create shape value!");
    }
    if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(global_env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(global_env, point.x, &napi_point[0])) != napi_ok ||
        (status=napi_create_double(global_env, point.y, &napi_point[1])) != napi_ok ||
        (status=napi_new_instance(global_env, constructor, 2, napi_point, &argv[1])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create cpVect point!");
    }
    if ((status=napi_create_double(global_env, distance, &argv[2])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create mass value!");
    }
    if ((status=napi_create_double(global_env, gradient.x, &napi_gradient[0])) != napi_ok ||
        (status=napi_create_double(global_env, gradient.y, &napi_gradient[1])) != napi_ok ||
        (status=napi_new_instance(global_env, constructor, 2, napi_gradient, &argv[3])) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't create cpVect gradient!");
    }
    ref_user_data = (napi_ref)data;
    if ((status=napi_get_reference_value(global_env, ref_user_data, &napi_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "invalid user data!");
    }
    argv[4] = napi_user_data;
    if ((status=napi_get_named_property(global_env, napi_user_data, PROP_NAME_SPACE_POINT_QUERY_FUNC,  &napi_callback_func)) != napi_ok) {
        napi_throw_error(global_env, NULL, "load callback func failed!");            
    }
    if ((status=napi_call_function(global_env, napi_value_global, napi_callback_func, argc, argv, &result)) != napi_ok) {
        napi_throw_error(global_env, NULL, "call callback failed!");
    }
}

napi_value
chipmunk_cpSpacePointQuery(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    napi_value napi_point_x, napi_point_y;
    cpVect point;
    cpFloat maxDistance;
    cpShapeFilter filter = CP_SHAPE_FILTER_NONE;
    uint32_t filter_group = CP_NO_GROUP;
    napi_value napi_filter_group;
    napi_value napi_filter_categories;
    napi_value napi_filter_mask;
    napi_status status;
    size_t argc = 6;
    napi_value argv[6];
    napi_ref ref_user_data;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_point_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_point_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_point_x, &point.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_point_y, &point.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid point!");
    }
    if ((status=napi_get_value_double(env, argv[2], &maxDistance) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid maxDistance!");
    }
    if ((status=napi_get_named_property(env, argv[3], "group", &napi_filter_group) != napi_ok) ||
        (status=napi_get_named_property(env, argv[3], "categories", &napi_filter_categories) != napi_ok) ||
        (status=napi_get_named_property(env, argv[3], "mask", &napi_filter_mask) != napi_ok) ||
        (status=napi_get_value_uint32(env, napi_filter_group, &filter_group) != napi_ok) ||
        (status=napi_get_value_uint32(env, napi_filter_categories, &filter.categories) != napi_ok) ||
        (status=napi_get_value_uint32(env, napi_filter_mask, &filter.mask) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid filter!");
    }
    filter.group = (cpGroup)filter_group;
    if ((status=napi_set_named_property(env, argv[5], PROP_NAME_SPACE_POINT_QUERY_FUNC,  argv[4])) != napi_ok) {
        napi_throw_error(env, NULL, "save callback func failed!");
    }    
    if ((status=napi_create_reference(env, argv[5], 1, &ref_user_data)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid userData!");
    }
    cpSpacePointQuery(space, point, maxDistance, filter, chipmunk_cpSpacePointQueryCallbackFunc, (void*)ref_user_data);
    if ((status=napi_delete_reference(env, ref_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "delete reference failed!");
    }
    return NULL;
}

#define PROP_NAME_SPACE_SHAPE_QUERY_FUNC ("__SPACE_SHAPE_QUERY_FUNC__")

void
chipmunk_cpSpaceShapeQueryCallbackFunc(cpShape* shape, cpContactPointSet* points, void* data)
{
    size_t argc = 3;
    napi_value argv[3], napi_callback_func;
    napi_value napi_value_global;
    napi_ref ref_user_data;
    napi_value napi_user_data, result;
    napi_status status;
    if (shape != NULL) {
        if ((status=napi_create_external(global_env, shape, NULL, NULL, &argv[0])) != napi_ok) {
            napi_throw_error(global_env, NULL, "can't create shape value!");
        }
        if ((status=napi_create_external(global_env, points, NULL, NULL, &argv[1])) != napi_ok) {
            napi_throw_error(global_env, NULL, "can't create points value!");
        }    
        if ((status=napi_get_global(global_env, &napi_value_global)) != napi_ok) {
            napi_throw_error(global_env, NULL, "can't get napi_value_global!");
        }
        ref_user_data = (napi_ref)data;
        if ((status=napi_get_reference_value(global_env, ref_user_data, &napi_user_data)) != napi_ok) {
            napi_throw_error(global_env, NULL, "invalid user data!");
        }    
        argv[2] = napi_user_data;
        if ((status=napi_get_named_property(global_env, napi_user_data, PROP_NAME_SPACE_SHAPE_QUERY_FUNC,  &napi_callback_func)) != napi_ok) {
            napi_throw_error(global_env, NULL, "load callback func failed!");
        }
        if ((status=napi_call_function(global_env, napi_value_global, napi_callback_func, argc, argv, &result)) != napi_ok) {
            napi_throw_error(global_env, NULL, "call callback failed!");
        }
    }
}

napi_value
chipmunk_cpSpaceShapeQuery(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 4;
    napi_value argv[4];
    napi_ref ref_user_data;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_set_named_property(env, argv[3], PROP_NAME_SPACE_SHAPE_QUERY_FUNC,  argv[2])) != napi_ok) {
        napi_throw_error(env, NULL, "save callback func failed!");
    }    
    if ((status=napi_create_reference(env, argv[3], 1, &ref_user_data)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid userData!");
    }
    cpSpaceShapeQuery(space, shape, chipmunk_cpSpaceShapeQueryCallbackFunc, (void*)ref_user_data);
    if ((status=napi_delete_reference(env, ref_user_data)) != napi_ok) {
        napi_throw_error(global_env, NULL, "delete reference failed!");
    }
    return NULL;
}

napi_value
chipmunk_cpSpaceAddBody(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    cpSpaceAddBody(space, body);
    return NULL;
}

napi_value
chipmunk_cpSpaceRemoveBody(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    cpSpaceRemoveBody(space, body);
    return NULL;
}

napi_value
chipmunk_cpSpaceReindexShapesForBody(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    cpSpaceReindexShapesForBody(space, body);
    return NULL;
}

napi_value
chipmunk_cpSpaceAddShape(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    cpSpaceAddShape(space, shape);
    return NULL;
}

napi_value
chipmunk_cpSpaceRemoveShape(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    cpSpaceRemoveShape(space, shape);
    return NULL;
}

napi_value
chipmunk_cpSpaceAddConstraint(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpConstraint* constraint = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    cpSpaceAddConstraint(space, constraint);
    return NULL;
}

napi_value
chipmunk_cpSpaceRemoveConstraint(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpConstraint* constraint = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    cpSpaceRemoveConstraint(space, constraint);
    return NULL;
}

napi_value
chipmunk_cpSpaceStep(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    cpFloat dt = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok ||
        (status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    if ((status=napi_get_value_double(env, argv[1], &dt) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid dt!");
    }
    cpSpaceStep(space, dt);
    return NULL;
}

napi_value
chipmunk_cpSpaceFree(napi_env env, napi_callback_info info)
{
    cpSpace* space = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok ||
        (status=napi_get_value_external(env, argv[0], (void**)&space)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid space!");
    }
    cpSpaceFree(space);
    return NULL;    
}

napi_value
chipmunk_cpSegmentShapeNew(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;    
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 4;
    napi_value argv[4];
    napi_value napi_a_x;
    napi_value napi_a_y;
    napi_value napi_b_x;
    napi_value napi_b_y;
    napi_value result;
    cpVect a, b;
    cpFloat radius;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_a_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_a_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_a_x, &a.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_a_y, &a.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid a!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_b_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_b_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_b_x, &b.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_b_y, &b.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid b!");
    }
    if ((status=napi_get_value_double(env, argv[3], &radius) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid radius!");
    }    
    shape = cpSegmentShapeNew(body, a, b, radius);
    if ((status=napi_create_external(env, shape, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpSegmentShapeGetA(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpVect a = cpvzero;
    napi_value napi_value_global, napi_a[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    a = cpSegmentShapeGetA(shape);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, a.x, &napi_a[0])) != napi_ok ||
        (status=napi_create_double(env, a.y, &napi_a[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_a, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect a!");
    }
    return result;    
}

napi_value
chipmunk_cpSegmentShapeGetB(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpVect b = cpvzero;
    napi_value napi_value_global, napi_b[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    b = cpSegmentShapeGetA(shape);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, b.x, &napi_b[0])) != napi_ok ||
        (status=napi_create_double(env, b.y, &napi_b[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_b, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect b!");
    }
    return result;    
}

napi_value
chipmunk_cpCircleShapeNew(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3];
    napi_value napi_offset_x;
    napi_value napi_offset_y;
    napi_value result;
    cpFloat radius;    
    cpVect offset;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_value_double(env, argv[1], &radius) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid radius!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_offset_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_offset_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_offset_x, &offset.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_offset_y, &offset.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid offset!");
    }
    shape = cpCircleShapeNew(body, radius, offset);
    if ((status=napi_create_external(env, shape, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpPolyShapeNewRaw(napi_env env, napi_callback_info info)
{
    cpBody* body = NULL;
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 3;
    napi_value argv[3];
    uint32_t i = 0, num_of_vertex = 0;
    cpVect* vertexs = NULL;    
    napi_value napi_vertex, napi_vertex_x, napi_vertex_y;
    cpFloat radius;
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body!");
    }
    if ((status=napi_get_array_length(env, argv[1], &num_of_vertex) != napi_ok) || num_of_vertex <= 0) {
        napi_throw_error(env, NULL, "invalid num of vertex!");
    }
    vertexs = alloca(num_of_vertex * sizeof(cpVect));
    if (vertexs == NULL) {
        napi_throw_error(env, NULL, "alloca vertexs failed!");
    }
    for (i=0; i<num_of_vertex; ++i) {
        napi_handle_scope scope;
        if ((status=napi_open_handle_scope(env, &scope)) != napi_ok) {
            napi_throw_error(env, NULL, "open handle scope failed!");
        }
        if ((status=napi_get_element(env, argv[1], i, &napi_vertex)) != napi_ok) {
            napi_throw_error(env, NULL, "get vertex failed!");
        }
        if ((status=napi_get_named_property(env, napi_vertex, "x", &napi_vertex_x) != napi_ok) ||
            (status=napi_get_named_property(env, napi_vertex, "y", &napi_vertex_y) != napi_ok) ||
            (status=napi_get_value_double(env, napi_vertex_x, &vertexs[i].x) != napi_ok) ||
            (status=napi_get_value_double(env, napi_vertex_y, &vertexs[i].y) != napi_ok)){
            napi_throw_error(env, NULL, "invalid vertex!");
        }
        if ((status=napi_close_handle_scope(env, scope)) != napi_ok) {
            napi_throw_error(env, NULL, "close handle scope failed!");
        }
    }
    if ((status=napi_get_value_double(env, argv[2], &radius) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid radius!");
    }
    shape = cpPolyShapeNewRaw(body, num_of_vertex, vertexs, radius);
    if ((status=napi_create_external(env, shape, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpCircleShapeGetRadius(napi_env env, napi_callback_info info)
{
    cpShape* circleShape = NULL;
    cpFloat radius = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&circleShape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid circleShape!");
    }
    radius = cpCircleShapeGetRadius(circleShape);
    if ((status=napi_create_double(env, radius, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create radius value!");
    }
    return result;
}

napi_value
chipmunk_cpCircleShapeSetRadius(napi_env env, napi_callback_info info)
{
    cpShape* circleShape = NULL;
    cpFloat radius = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&circleShape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid circleShape!");
    }
    if ((status=napi_get_value_double(env, argv[1], &radius) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid radius!");
    }
    cpCircleShapeSetRadius(circleShape, radius);
    return NULL;
}

napi_value
chipmunk_cpCircleShapeGetOffset(napi_env env, napi_callback_info info)
{
    cpShape* circleShape = NULL;
    cpVect offset = cpvzero;
    napi_value napi_value_global, napi_offset[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&circleShape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid circleShape!");
    }
    offset = cpCircleShapeGetOffset(circleShape);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, offset.x, &napi_offset[0])) != napi_ok ||
        (status=napi_create_double(env, offset.y, &napi_offset[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_offset, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect offset!");
    }
    return result;
}

napi_value
chipmunk_cpCircleShapeSetOffset(napi_env env, napi_callback_info info)
{
    cpShape* circleShape = NULL;
    cpVect offset = cpvzero;
    napi_value napi_offset_x;
    napi_value napi_offset_y;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&circleShape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid circleShape!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_offset_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_offset_y) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_offset_x, &offset.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_offset_y, &offset.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid offset!");
    }
    cpCircleShapeSetOffset(circleShape, offset);
    return NULL;
}

napi_value
chipmunk_cpShapeGetSpace(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpSpace* space = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    space = cpShapeGetSpace(shape);
    if ((status=napi_create_external(env, space, NULL, NULL, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create space value!");
    }
    return result;
}

napi_value
chipmunk_cpShapeGetBody(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    body = cpShapeGetBody(shape);
    if ((status=napi_create_external(env, body, NULL, NULL, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create body value!");
    }
    return result;
}

napi_value
chipmunk_cpShapeSetBody(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpBody* body = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2], napi_value_null;
    _Bool is_null = cpFalse;    
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_get_null(global_env, &napi_value_null)) != napi_ok) {
        napi_throw_error(global_env, NULL, "can't get napi_value_null!");
    }    
    if ((status=napi_strict_equals(env, argv[1], napi_value_null, &is_null)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body value!");
    }
    if (!is_null) {        
        if ((status=napi_get_value_external(env, argv[1], (void**)&body) != napi_ok)) {
            napi_throw_error(env, NULL, "invalid body!");
        }
    }
    cpShapeSetBody(shape, body);
    return NULL;
}

napi_value
chipmunk_cpShapeGetFilter(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpShapeFilter filter = CP_SHAPE_FILTER_NONE;
    napi_value napi_value_global, napi_filter[3];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    filter = cpShapeGetFilter(shape);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(env, napi_value_global, "cpShapeFilter", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpShapeFilter!");
    }
    if ((status=napi_create_uint32(env, filter.group, &napi_filter[0])) != napi_ok ||
        (status=napi_create_uint32(env, filter.categories, &napi_filter[1])) != napi_ok ||
        (status=napi_create_uint32(env, filter.mask, &napi_filter[2])) != napi_ok ||        
        (status=napi_new_instance(env, constructor, 3, napi_filter, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpShapeFilter!");
    }
    return result;
}

napi_value
chipmunk_cpShapeSetFilter(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpShapeFilter filter = CP_SHAPE_FILTER_NONE;
    uint32_t filter_group = CP_NO_GROUP;
    napi_value napi_filter_group;
    napi_value napi_filter_categories;
    napi_value napi_filter_mask;    
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_get_named_property(env, argv[1], "group", &napi_filter_group) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "categories", &napi_filter_categories) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "mask", &napi_filter_mask) != napi_ok) ||
        (status=napi_get_value_uint32(env, napi_filter_group, &filter_group) != napi_ok) ||
        (status=napi_get_value_uint32(env, napi_filter_categories, &filter.categories) != napi_ok) ||
        (status=napi_get_value_uint32(env, napi_filter_mask, &filter.mask) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid filter!");
    }
    filter.group = (cpGroup)filter_group;
    cpShapeSetFilter(shape, filter);
    return NULL;
}

napi_value
chipmunk_cpShapeGetCollisionType(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    uint32_t collisionType = (uint32_t)CP_WILDCARD_COLLISION_TYPE;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    collisionType = (uint32_t)cpShapeGetCollisionType(shape);
    if ((status=napi_create_uint32(env, collisionType, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create collisionType value!");
    }
    return result;
}

napi_value
chipmunk_cpShapeSetCollisionType(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    uint32_t collisionType = (uint32_t)CP_WILDCARD_COLLISION_TYPE;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_get_value_uint32(env, argv[1], &collisionType) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid collisionType!");
    }
    cpShapeSetCollisionType(shape, (cpCollisionType)collisionType);
    return NULL;
}

napi_value
chipmunk_cpShapeGetElasticity(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpFloat elasticity = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    elasticity = cpShapeGetElasticity(shape);
    if ((status=napi_create_double(env, elasticity, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create elasticity value!");
    }
    return result;
}

napi_value
chipmunk_cpShapeSetElasticity(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpFloat elasticity = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_get_value_double(env, argv[1], &elasticity) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid elasticity!");
    }
    cpShapeSetElasticity(shape, elasticity);
    return NULL;
}

napi_value
chipmunk_cpArbiterSetElasticity(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    cpFloat elasticity = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    if ((status=napi_get_value_double(env, argv[1], &elasticity) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid elasticity!");
    }
    cpArbiterSetRestitution(arbiter, elasticity);
    return NULL;
}

napi_value
chipmunk_cpArbiterSetFriction(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    cpFloat friction = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    if ((status=napi_get_value_double(env, argv[1], &friction) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid friction!");
    }
    cpArbiterSetFriction(arbiter, friction);
    return NULL;
}

napi_value
chipmunk_cpArbiterSetSurfaceVelocity(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    cpVect velocity = cpvzero;
    napi_value napi_velocity_x;
    napi_value napi_velocity_y;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    if ((status=napi_get_named_property(env, argv[1], "x", &napi_velocity_x) != napi_ok) ||
        (status=napi_get_named_property(env, argv[1], "y", &napi_velocity_y) != napi_ok) ||
        (status=napi_get_value_double(env, napi_velocity_x, &velocity.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_velocity_y, &velocity.y) != napi_ok)){
        napi_throw_error(env, NULL, "invalid velocity!");
    }
    cpArbiterSetSurfaceVelocity(arbiter, velocity);
    return NULL;
}

napi_value
chipmunk_cpShapeGetFriction(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpFloat friction = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    friction = cpShapeGetFriction(shape);
    if ((status=napi_create_double(env, friction, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create friction value!");
    }
    return result;
}

napi_value
chipmunk_cpShapeSetSensor(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    _Bool sensor = false;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_get_value_bool(env, argv[1], &sensor) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid sensor!");
    }
    cpShapeSetSensor(shape, (cpBool)sensor);
    return NULL;
}

napi_value
chipmunk_cpShapeGetSensor(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    _Bool sensor = false;
    napi_status status;
    size_t argc = 1;
    napi_value napi_value_true, napi_value_false, argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    sensor = (_Bool)cpShapeGetSensor(shape);
    if ((status=napi_get_boolean(env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_true!");
    }
    if ((status=napi_get_boolean(env, cpFalse, &napi_value_false)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_false!");
    }
    result = (sensor ? napi_value_true : napi_value_false);
    return result;
}

napi_value
chipmunk_cpShapeSetFriction(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    cpFloat friction = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    if ((status=napi_get_value_double(env, argv[1], &friction) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid friction!");
    }
    cpShapeSetFriction(shape, friction);
    return NULL;
}

napi_value
chipmunk_cpShapeFree(napi_env env, napi_callback_info info)
{
    cpShape* shape = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok ||
        (status=napi_get_value_external(env, argv[0], (void**)&shape)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid shape!");
    }
    cpShapeFree(shape);
    return NULL;
}

napi_value
chipmunk_cpSlideJointNew(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpBody* body_a = NULL;
    cpBody* body_b = NULL;
    cpVect anchor_a, anchor_b;
    cpFloat min, max;
    napi_status status;
    size_t argc = 6;
    napi_value argv[6];    
    napi_value napi_anchor_a[2], napi_anchor_b[2];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body_a)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body a!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&body_b)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body b!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_anchor_a[0]) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_anchor_a[0]) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_anchor_a[0], &anchor_a.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_anchor_a[0], &anchor_a.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid anchor a!");
    }
    if ((status=napi_get_named_property(env, argv[3], "x", &napi_anchor_b[0]) != napi_ok) ||
        (status=napi_get_named_property(env, argv[3], "y", &napi_anchor_b[0]) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_anchor_b[0], &anchor_b.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_anchor_b[0], &anchor_b.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid anchor b!");
    }
    if ((status=napi_get_value_double(env, argv[4], &min) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid min!");
    }
    if ((status=napi_get_value_double(env, argv[5], &max) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid max!");
    }
    constraint = cpSlideJointNew(body_a, body_b, anchor_a, anchor_b, min, max);
    if ((status=napi_create_external(env, constraint, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpPivotJointNew2(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpBody* body_a = NULL;
    cpBody* body_b = NULL;
    cpVect anchor_a, anchor_b;
    napi_status status;
    size_t argc = 4;
    napi_value argv[4];
    napi_value napi_anchor_a[2], napi_anchor_b[2];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&body_a)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body a!");
    }
    if ((status=napi_get_value_external(env, argv[1], (void**)&body_b)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid body b!");
    }
    if ((status=napi_get_named_property(env, argv[2], "x", &napi_anchor_a[0]) != napi_ok) ||
        (status=napi_get_named_property(env, argv[2], "y", &napi_anchor_a[0]) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_anchor_a[0], &anchor_a.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_anchor_a[0], &anchor_a.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid anchor a!");
    }
    if ((status=napi_get_named_property(env, argv[3], "x", &napi_anchor_b[0]) != napi_ok) ||
        (status=napi_get_named_property(env, argv[3], "y", &napi_anchor_b[0]) != napi_ok) ||        
        (status=napi_get_value_double(env, napi_anchor_b[0], &anchor_b.x) != napi_ok) ||
        (status=napi_get_value_double(env, napi_anchor_b[0], &anchor_b.y) != napi_ok)) {
        napi_throw_error(env, NULL, "invalid anchor b!");
    }
    constraint = cpPivotJointNew2(body_a, body_b, anchor_a, anchor_b);
    if ((status=napi_create_external(env, constraint, NULL, NULL, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpConstraintSetUserData(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    napi_ref result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    if ((status=napi_create_reference(env, argv[1], 0, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid userData!");
    }
    cpConstraintSetUserData(constraint, (cpDataPointer)result);
    return NULL;
}

napi_value
chipmunk_cpConstraintGetUserData(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_ref ref;
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    ref = (napi_ref)cpConstraintGetUserData(constraint);
    if ((status=napi_get_reference_value(env, ref, &result)) != napi_ok) {
        return NULL;
    }
    return result;
}

napi_value
chipmunk_cpConstraintFree(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok ||
        (status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    cpConstraintFree(constraint);
    return NULL;    
}

napi_value
chipmunk_cpConstraintSetMaxBias(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpFloat maxBias = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    if ((status=napi_get_value_double(env, argv[1], &maxBias) != napi_ok)){
        napi_throw_error(env, NULL, "invalid maxBias!");
    }
    cpConstraintSetMaxBias(constraint, maxBias);
    return NULL;
}

napi_value
chipmunk_cpConstraintGetMaxBias(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpFloat maxBias = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    maxBias = cpConstraintGetMaxBias(constraint);
    if ((status=napi_create_double(env, maxBias, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create maxBias value!");
    }
    return result;
}

napi_value
chipmunk_cpConstraintSetMaxForce(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpFloat maxForce = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    if ((status=napi_get_value_double(env, argv[1], &maxForce) != napi_ok)){
        napi_throw_error(env, NULL, "invalid maxForce!");
    }
    cpConstraintSetMaxForce(constraint, maxForce);
    return NULL;
}

napi_value
chipmunk_cpConstraintGetMaxForce(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpFloat maxForce = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    maxForce = cpConstraintGetMaxForce(constraint);
    if ((status=napi_create_double(env, maxForce, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create maxForce value!");
    }
    return result;
}

napi_value
chipmunk_cpConstraintSetErrorBias(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpFloat errorBias = 0.0;
    napi_status status;
    size_t argc = 2;
    napi_value argv[2];
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    if ((status=napi_get_value_double(env, argv[1], &errorBias) != napi_ok)){
        napi_throw_error(env, NULL, "invalid errorBias!");
    }
    cpConstraintSetErrorBias(constraint, errorBias);
    return NULL;
}

napi_value
chipmunk_cpConstraintGetErrorBias(napi_env env, napi_callback_info info)
{
    cpConstraint* constraint = NULL;
    cpFloat errorBias = 0.0;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&constraint)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid constraint!");
    }
    errorBias = cpConstraintGetErrorBias(constraint);
    if ((status=napi_create_double(env, errorBias, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create errorBias value!");
    }
    return result;
}

napi_value
chipmunk_cpArbiterGetShapes(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    cpShape* shape_a = NULL;
    cpShape* shape_b = NULL;    
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value napi_shape_a, napi_shape_b, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    cpArbiterGetShapes(arbiter, &shape_a, &shape_b);
    if ((status=napi_create_external(env, shape_a, NULL, NULL, &napi_shape_a)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create shape a value!");
    }
    if ((status=napi_create_external(env, shape_b, NULL, NULL, &napi_shape_b)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create shape b value!");
    }
    if ((status=napi_create_object(env, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create result!");
    }
    if ((status=napi_set_named_property(env, result, "shapeA", napi_shape_a)) != napi_ok) {
        napi_throw_error(env, NULL, "set shape a failed!");            
    }
    if ((status=napi_set_named_property(env, result, "shapeB", napi_shape_b)) != napi_ok) {
        napi_throw_error(env, NULL, "set shape b failed!");
    }
    return result;
}

napi_value
chipmunk_cpArbiterGetBodies(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    cpBody* body_a = NULL;
    cpBody* body_b = NULL;    
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value napi_body_a, napi_body_b, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    cpArbiterGetBodies(arbiter, &body_a, &body_b);
    if ((status=napi_create_external(env, body_a, NULL, NULL, &napi_body_a)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create body a value!");
    }
    if ((status=napi_create_external(env, body_b, NULL, NULL, &napi_body_b)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create body b value!");
    }
    if ((status=napi_create_object(env, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create result!");
    }
    if ((status=napi_set_named_property(env, result, "bodyA", napi_body_a)) != napi_ok) {
        napi_throw_error(env, NULL, "set body a failed!");            
    }
    if ((status=napi_set_named_property(env, result, "bodyB", napi_body_b)) != napi_ok) {
        napi_throw_error(env, NULL, "set body b failed!");
    }
    return result;
}

napi_value
chipmunk_cpArbiterGetNormal(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    cpVect normal = cpvzero;
    napi_value napi_value_global, napi_normal[2];
    napi_status status;
    size_t argc = 1;
    napi_value argv[1];
    napi_value constructor, result;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    normal = cpArbiterGetNormal(arbiter);
    if ((status=napi_get_global(env, &napi_value_global)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_global!");
    }
    if ((status=napi_get_named_property(env, napi_value_global, "cpVect", &constructor)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get cpVect!");
    }
    if ((status=napi_create_double(env, normal.x, &napi_normal[0])) != napi_ok ||
        (status=napi_create_double(env, normal.y, &napi_normal[1])) != napi_ok ||
        (status=napi_new_instance(env, constructor, 2, napi_normal, &result)) != napi_ok) {
        napi_throw_error(env, NULL, "can't create cpVect normal!");
    }
    return result;
}

napi_value
chipmunk_cpArbiterIgnore(napi_env env, napi_callback_info info)
{
    cpArbiter* arbiter = NULL;
    napi_status status;
    size_t argc = 1;
    napi_value argv[1], napi_value_true, napi_value_false;
    if ((status=napi_get_cb_info(env, info, &argc, argv, NULL, NULL)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid num of arguments!");        
    }
    if ((status=napi_get_value_external(env, argv[0], (void**)&arbiter)) != napi_ok) {
        napi_throw_error(env, NULL, "invalid arbiter!");
    }
    if ((status=napi_get_boolean(env, cpTrue, &napi_value_true)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_true!");
    }
    if ((status=napi_get_boolean(env, cpFalse, &napi_value_false)) != napi_ok) {
        napi_throw_error(env, NULL, "can't get napi_value_true!");
    }    
    return (cpArbiterIgnore(arbiter) ? napi_value_true : napi_value_false);
}

napi_value chipmunk_napi_init(napi_env env, napi_value exports)
{
    napi_status status;
    global_env = env;
    napi_property_descriptor descriptors[] = {
        { "cpBodyNew",                       NULL, chipmunk_cpBodyNew,                   NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyNewStatic",                 NULL, chipmunk_cpBodyNewStatic,             NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyNewKinematic",              NULL, chipmunk_cpBodyNewKinematic,          NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyFree",                      NULL, chipmunk_cpBodyFree,                  NULL, NULL, NULL, napi_default, NULL },        
        { "cpBodySetUserData",               NULL, chipmunk_cpBodySetUserData,           NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetUserData",               NULL, chipmunk_cpBodyGetUserData,           NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetPosition",               NULL, chipmunk_cpBodyGetPosition,           NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetPosition",               NULL, chipmunk_cpBodySetPosition,           NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetVelocity",               NULL, chipmunk_cpBodyGetVelocity,           NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetVelocity",               NULL, chipmunk_cpBodySetVelocity,           NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetMass",                   NULL, chipmunk_cpBodyGetMass,               NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetMass",                   NULL, chipmunk_cpBodySetMass,               NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetAngle",                  NULL, chipmunk_cpBodyGetAngle,              NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetAngle",                  NULL, chipmunk_cpBodySetAngle,              NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetAngularVelocity",        NULL, chipmunk_cpBodySetAngularVelocity,    NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetAngularVelocity",        NULL, chipmunk_cpBodyGetAngularVelocity,    NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyGetType",                   NULL, chipmunk_cpBodyGetType,               NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetType",                   NULL, chipmunk_cpBodySetType,               NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyIsSleeping",                NULL, chipmunk_cpBodyIsSleeping,            NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyActivate",                  NULL, chipmunk_cpBodyActivate,              NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySleep",                     NULL, chipmunk_cpBodySleep,                 NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyUpdateVelocity",            NULL, chipmunk_cpBodyUpdateVelocity,        NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetVelocityUpdateFunc",     NULL, chipmunk_cpBodySetVelocityUpdateFunc, NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyUpdatePosition",            NULL, chipmunk_cpBodyUpdatePosition,        NULL, NULL, NULL, napi_default, NULL },
        { "cpBodySetPositionUpdateFunc",     NULL, chipmunk_cpBodySetPositionUpdateFunc, NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyApplyForceAtWorldPoint",    NULL, chipmunk_cpBodyApplyForceAtWorldPoint, NULL, NULL, NULL, napi_default, NULL },        
        { "cpBodyApplyForceAtLocalPoint",    NULL, chipmunk_cpBodyApplyForceAtLocalPoint, NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyApplyImpulseAtWorldPoint",  NULL, chipmunk_cpBodyApplyImpulseAtWorldPoint, NULL, NULL, NULL, napi_default, NULL },
        { "cpBodyApplyImpulseAtLocalPoint",  NULL, chipmunk_cpBodyApplyImpulseAtLocalPoint, NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceNew",                      NULL, chipmunk_cpSpaceNew,                  NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceContainsBody",             NULL, chipmunk_cpSpaceContainBody,          NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceContainsShape",            NULL, chipmunk_cpSpaceContainShape,         NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceAddBody",                  NULL, chipmunk_cpSpaceAddBody,              NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceRemoveBody",               NULL, chipmunk_cpSpaceRemoveBody,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceReindexShapesForBody",     NULL, chipmunk_cpSpaceReindexShapesForBody, NULL, NULL, NULL, napi_default, NULL },        
        { "cpSpaceAddShape",                 NULL, chipmunk_cpSpaceAddShape,             NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceRemoveShape",              NULL, chipmunk_cpSpaceRemoveShape,          NULL, NULL, NULL, napi_default, NULL },        
        { "cpSpaceAddConstraint",            NULL, chipmunk_cpSpaceAddConstraint,        NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceRemoveConstraint",         NULL, chipmunk_cpSpaceRemoveConstraint,     NULL, NULL, NULL, napi_default, NULL },        
        { "cpSpaceStep",                     NULL, chipmunk_cpSpaceStep,                 NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceSetIterations",            NULL, chipmunk_cpSpaceSetIterations,        NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetIterations",            NULL, chipmunk_cpSpaceGetIterations,        NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceSetIdleSpeedThreshold",    NULL, chipmunk_cpSpaceSetIdleSpeedThreshold,NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetIdleSpeedThreshold",    NULL, chipmunk_cpSpaceGetIdleSpeedThreshold,NULL, NULL, NULL, napi_default, NULL },        
        { "cpSpaceSetSleepTimeThreshold",    NULL, chipmunk_cpSpaceSetSleepTimeThreshold,NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetSleepTimeThreshold",    NULL, chipmunk_cpSpaceGetSleepTimeThreshold,NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceSetCollisionSlop",         NULL, chipmunk_cpSpaceSetCollisionSlop,     NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetCollisionSlop",         NULL, chipmunk_cpSpaceGetCollisionSlop,     NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceSetCollisionBias",         NULL, chipmunk_cpSpaceSetCollisionBias,     NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetCollisionBias",         NULL, chipmunk_cpSpaceGetCollisionBias,     NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceSetGravity",               NULL, chipmunk_cpSpaceSetGravity,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetGravity",               NULL, chipmunk_cpSpaceGetGravity,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceSetDamping",               NULL, chipmunk_cpSpaceSetDamping,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetDamping",               NULL, chipmunk_cpSpaceGetDamping,           NULL, NULL, NULL, napi_default, NULL },        
        { "cpSpaceSetUserData",              NULL, chipmunk_cpSpaceSetUserData,          NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceGetUserData",              NULL, chipmunk_cpSpaceGetUserData,          NULL, NULL, NULL, napi_default, NULL },        
        { "cpSpaceAddPostStepCallback",      NULL, chipmunk_cpSpaceAddPostStepCallback,  NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceAddCollisionHandler",      NULL, chipmunk_cpSpaceAddCollisionHandler,  NULL, NULL, NULL, napi_default, NULL },
        { "cpSpacePointQuery",               NULL, chipmunk_cpSpacePointQuery,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceShapeQuery",               NULL, chipmunk_cpSpaceShapeQuery,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSpaceFree",                     NULL, chipmunk_cpSpaceFree,                 NULL, NULL, NULL, napi_default, NULL },        
        { "cpSegmentShapeNew",               NULL, chipmunk_cpSegmentShapeNew,           NULL, NULL, NULL, napi_default, NULL },
        { "cpSegmentShapeGetA",              NULL, chipmunk_cpSegmentShapeGetA,          NULL, NULL, NULL, napi_default, NULL },
        { "cpSegmentShapeGetB",              NULL, chipmunk_cpSegmentShapeGetB,          NULL, NULL, NULL, napi_default, NULL },                
        { "cpCircleShapeNew",                NULL, chipmunk_cpCircleShapeNew,            NULL, NULL, NULL, napi_default, NULL },
        { "cpPolyShapeNewRaw",               NULL, chipmunk_cpPolyShapeNewRaw,           NULL, NULL, NULL, napi_default, NULL },
        { "cpCircleShapeGetRadius",          NULL, chipmunk_cpCircleShapeGetRadius,      NULL, NULL, NULL, napi_default, NULL },
        { "cpCircleShapeSetRadius",          NULL, chipmunk_cpCircleShapeSetRadius,      NULL, NULL, NULL, napi_default, NULL },
        { "cpCircleShapeGetOffset",          NULL, chipmunk_cpCircleShapeGetOffset,      NULL, NULL, NULL, napi_default, NULL },
        { "cpCircleShapeSetOffset",          NULL, chipmunk_cpCircleShapeSetOffset,      NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetSpace",                 NULL, chipmunk_cpShapeGetSpace,             NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeSetBody",                  NULL, chipmunk_cpShapeSetBody,              NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetBody",                  NULL, chipmunk_cpShapeGetBody,              NULL, NULL, NULL, napi_default, NULL },        
        { "cpShapeSetFilter",                NULL, chipmunk_cpShapeSetFilter,            NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetFilter",                NULL, chipmunk_cpShapeGetFilter,            NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeSetCollisionType",         NULL, chipmunk_cpShapeSetCollisionType,     NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetCollisionType",         NULL, chipmunk_cpShapeGetCollisionType,     NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetElasticity",            NULL, chipmunk_cpShapeGetElasticity,        NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeSetElasticity",            NULL, chipmunk_cpShapeSetElasticity,        NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetFriction",              NULL, chipmunk_cpShapeGetFriction,          NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeSetFriction",              NULL, chipmunk_cpShapeSetFriction,          NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeGetSensor",                NULL, chipmunk_cpShapeGetSensor,            NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeSetSensor",                NULL, chipmunk_cpShapeSetSensor,            NULL, NULL, NULL, napi_default, NULL },
        { "cpShapeFree",                     NULL, chipmunk_cpShapeFree,                 NULL, NULL, NULL, napi_default, NULL },        
        { "cpSlideJointNew",                 NULL, chipmunk_cpSlideJointNew,             NULL, NULL, NULL, napi_default, NULL },
        { "cpPivotJointNew2",                NULL, chipmunk_cpPivotJointNew2,            NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintSetUserData",         NULL, chipmunk_cpConstraintSetUserData,     NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintGetUserData",         NULL, chipmunk_cpConstraintGetUserData,     NULL, NULL, NULL, napi_default, NULL },        
        { "cpConstraintGetMaxBias",          NULL, chipmunk_cpConstraintGetMaxBias,      NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintSetMaxBias",          NULL, chipmunk_cpConstraintSetMaxBias,      NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintGetMaxForce",         NULL, chipmunk_cpConstraintGetMaxForce,     NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintSetMaxForce",         NULL, chipmunk_cpConstraintSetMaxForce,     NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintGetErrorBias",        NULL, chipmunk_cpConstraintGetErrorBias,    NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintSetErrorBias",        NULL, chipmunk_cpConstraintSetErrorBias,    NULL, NULL, NULL, napi_default, NULL },
        { "cpConstraintFree",                NULL, chipmunk_cpConstraintFree,            NULL, NULL, NULL, napi_default, NULL },        
        { "cpArbiterGetShapes",              NULL, chipmunk_cpArbiterGetShapes,          NULL, NULL, NULL, napi_default, NULL },
        { "cpArbiterGetBodies",              NULL, chipmunk_cpArbiterGetBodies,          NULL, NULL, NULL, napi_default, NULL },        
        { "cpArbiterGetNormal",              NULL, chipmunk_cpArbiterGetNormal,          NULL, NULL, NULL, napi_default, NULL },
        { "cpArbiterIgnore",                 NULL, chipmunk_cpArbiterIgnore,             NULL, NULL, NULL, napi_default, NULL },
        { "cpArbiterSetElasticity",          NULL, chipmunk_cpArbiterSetElasticity,      NULL, NULL, NULL, napi_default, NULL },
        { "cpArbiterSetFriction",            NULL, chipmunk_cpArbiterSetFriction,        NULL, NULL, NULL, napi_default, NULL },
        { "cpArbiterSetSurfaceVelocity",     NULL, chipmunk_cpArbiterSetSurfaceVelocity, NULL, NULL, NULL, napi_default, NULL }
    };
    status = napi_define_properties(env, exports, sizeof(descriptors)/sizeof(descriptors[0]), descriptors);
    if (status != napi_ok) {
        napi_throw_error(env, NULL, "chipmunk napi init failed!");
        return NULL;
    }
    return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, chipmunk_napi_init);
