{
    "targets": [{
        "target_name": "chipmunk_napi",
        'direct_dependent_settings': {
            'defines': [
            ],
            'linkflags': [
            ],
        },
        'include_dirs': [
            "inc/chipmunk"
        ],
        'link_settings': {
            "libraries": [
                "-lchipmunk"
            ],
            "library_dirs": [
                "bin/"
            ],
        },
        "sources": [
            "src/chipmunk_napi.c"
        ]
    }]
}
